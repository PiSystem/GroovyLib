package tech.teslex.gr8lib;

import cn.nukkit.plugin.PluginBase;

public class GroovyLib extends PluginBase {

	public static String GROOVY_VERSION = "2.5.6";

	@Override
	public void onEnable() {
		getLogger().info("Groovy Version: §e" + GROOVY_VERSION);
	}
}