# GroovyLib

Groovy Library for NukkitX

> **Groovy Version: `2.5.6`**

> [**Download**](build/libs)

## Create jar:
```shell
./gradlew clean shadow
```
